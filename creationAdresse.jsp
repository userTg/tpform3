<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>adresse</title>
<link type="text/css" rel="stylesheet" href="formulaire.css">
</head>
<body>

<c:import url="creationClient.jsp"></c:import>
<br>
	<form method="post" action="creationAdresse3">
<fieldset class="scheduler-border">
	<legend class="scheduler-border">Inscrivez-vous</legend>
		<div><p class="titre">Formulaire de création d'une adresse</p></div>
		<div>
			<label for="rue"> Rue * :</label> <input type="text" id="rue"
				name="rue" value="" />
		</div>
		<div>
			<label for="codePostal"> Code postal * :</label> <input type="text"
				id="codePostal" name="codePostal" value="" />
		</div>
		<div>
			<label for="ville"> Ville * :</label> <input type="text"
				id="ville" name="ville" value="" />
		</div>
		<p class="bouton"> <input type="submit" value="Enregistrer" /></p>
		</fieldset>
	</form>
</body>
</html>