package org.eclipse.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.eclipse.model.Client;

/**
 * Servlet implementation class CreationClientServlet
 */
@WebServlet("/creationClient3") 
public class CreationClientServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	public static final String CREATION_CLIENT = "/WEB-INF/creationClient.jsp";
	public static final String AFFICHER_CLIENT = "/WEB-INF/afficherClient.jsp";
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CreationClientServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		this.getServletContext().getRequestDispatcher(CREATION_CLIENT).forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		String message = null;
		String nom = request.getParameter("nom");
		String prenom = request.getParameter("prenom");
		String tel = request.getParameter("tel");
		String email = request.getParameter("email");
		Client client = new Client();
		client.setNom(nom);
		client.setPrenom(prenom);
		client.setTel(tel);
		client.setEmail(email);
		
		if (nom.trim().isEmpty() || prenom.trim().isEmpty() || tel.trim().isEmpty() || email.isEmpty() ) {
			message = "Erreur - Vous n'avez pas rempli tous les champs obligatoires. <br> <a href=\"creationClient\">Cliquez ici</a> pour accéder au formulaire de création d'une commande.";
		} else {
			 message = "Commande créée avec succès !";
		}
		session.setAttribute("client", client);
		request.setAttribute("message", message);
		this.getServletContext().getRequestDispatcher(AFFICHER_CLIENT).forward(request, response);
	}

}
