package org.eclipse.controller;

import java.io.IOException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.eclipse.model.Adresse;
import org.eclipse.model.Client;

/**
 * Servlet implementation class CreationAdresseServlet3
 */
@WebServlet("/creationAdresse3")
public class CreationAdresseServlet3 extends HttpServlet {
	private static final long serialVersionUID = 1L;
	public static final String CREATION_ADRESSE = "/WEB-INF/creationAdresse.jsp";
	public static final String AFFICHER_ADRESSE = "/WEB-INF/afficherAdresse.jsp";
	/**
	 * @see Servlet#init(ServletConfig)
	 */
	
	 public CreationAdresseServlet3() {
	        super();
	        // TODO Auto-generated constructor stub
	    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		this.getServletContext().getRequestDispatcher(CREATION_ADRESSE).forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session = request.getSession();
		String message = null;
	    
	    Client monClient = (Client) session.getAttribute("client");
		Adresse adresse = new Adresse();
		String rue = request.getParameter("rue");
		String codePostal = request.getParameter("codePostal");
		String ville = request.getParameter("ville");
		
		adresse.setRue(rue);
		adresse.setCodePostal(codePostal);
		adresse.setVille(ville);
		adresse.setClient(monClient);
		adresse.getClient().setNom(monClient.getNom());
		adresse.getClient().setPrenom(monClient.getPrenom());
		adresse.getClient().setTel(monClient.getTel());
		adresse.getClient().setEmail(monClient.getEmail());
		
		
		if (monClient.getNom().trim().isEmpty() || monClient.getPrenom().trim().isEmpty() || monClient.getTel().trim().isEmpty() 
				|| monClient.getEmail().isEmpty() || rue.isEmpty() || codePostal.isEmpty() || ville.isEmpty()) {
			message = "Erreur - Vous n'avez pas rempli tous les champs obligatoires. <br> <a href=\"creationClient\">Cliquez ici</a> pour accéder au formulaire de création d'une commande.";
		} else {
			 message = "Commande créée avec succès !";
		}
		
		request.setAttribute("message", message);
		request.setAttribute("adresse", adresse);
		
		this.getServletContext().getRequestDispatcher(AFFICHER_ADRESSE).forward(request, response);
	}

}
